#!/usr/bin/env python3
#
# Create a test PostgreSQL DB; populate it with test data.
#
# This script may be called on its own (as a stand-alone script) to create a
# stand-alone test PostgreSQL DB; or it may be called by another script such
# as `solution_q1.py` or `test_solution_q1.py`, to create a test PostgreSQL DB
# that is used by that other script.
#
# Usage as a stand-alone script:
#   python3 create_test_db.py
#
# Note: Because this script is simply churning out a bunch of test data,
# I'm allowing myself to write terser Python code than I normally would.
# (Lots of list comprehensions nested within `zip`, `join`, etc.)

import os
import psycopg2
import sys


# The default filename of the test PostgreSQL DB.
#
# (If this were a longer-lived script, I'd set up command-line parsing
# using `argparse`, so this could be specified on the command-line.)
_TEST_DB_NAME = 'lotsearch_test'


# What I know about the production DB:
#
#       The provided csv --- "Dataset_update.csv" contains a list of datasets
#       and Pkeys(Primary Keys) that correspond to tables in a database.
#       with a "QA" field that needs to be updated to a value of 3 .
#
#       Write a python script that reads the tables(dataset) and
#       primary key(pkeys) columns from the csv and updates the QA column
#       for the corresponding pkeys.
#
# For testing, we'll take the list of tables, columns, and primary keys
# from the spec; then we'll add in a few dummy table names, and a few
# dummy columns, and a few dummy primary keys; then we'll calculate
# a Cartesian product of (tables x pkeys) to produce our test data.
#
# So, the production DB contains (at least) the following tables
# (this list extracted from spec file `input/Dataset_update.csv`):
_TABLES_IN_SPEC = [
        'EPAAUDITS',
        'EPAConSites',
        'EPADepot',
        'EPANOTICES',
        'EPAPublicReg',
        'EPAWASTE',
        'NationalLiquidFuels',
        'SWRRIPF',
]
_DUMMY_TABLES = ['DummyTableA', 'DummyTableB']
_TABLES_TO_TEST = _TABLES_IN_SPEC + _DUMMY_TABLES


# and the following integer primary keys
# (this list extracted from spec file `input/Dataset_update.csv`):
_PKEYS_IN_SPEC = [
        39,
        95,
        461,
        656,
        736,
        841,
        898,
        977,
        1417,
        1812,
        1813,
        2178,
        2223,
        2294,
        2451,
        2453,
        2578,
        2822,
        3608,
        5472,
        5801,
        9914,
        10197,
        10198,
        10969,
        12662,
        12663,
        12667,
        14407,
        68696,  # (This won't fit into 16 bits; pkey must be `int` or larger.)
]
_DUMMY_PKEYS = [7, 8, 9]
_PKEYS_TO_TEST = _PKEYS_IN_SPEC + _DUMMY_PKEYS


# We'll need our tables to have the following columns:
_COLUMNS_IN_SPEC = [
        # column name & type,           # list of initial values
        ('Pkey integer primary key',    _PKEYS_TO_TEST),
        ('QA integer',                  [111 for pkey in _PKEYS_TO_TEST]),
]
_DUMMY_COLUMNS = [
        # column name & type,           # list of initial values
        ('DummyField1 varchar(20)',     ['dummy' for pkey in _PKEYS_TO_TEST]),
        ('DummyField2 integer',         [222 for pkey in _PKEYS_TO_TEST]),
]
_COLUMNS_TO_TEST = _COLUMNS_IN_SPEC + _DUMMY_COLUMNS


def list_tables_to_test():
    """Return a list of the names (Python strings) of the tables to test."""
    # Return a copy, so it can't accidentally be modified.
    return _TABLES_TO_TEST[:]


def create_new_test_db(db_name=None, *,
        execute_sql=True, drop_existing_tables=False):
    """Create a new PostgreSQL test DB named `db_name`.

    If specified `db_name` is `None`, use the default `_TEST_DB_NAME`.

    If `execute_sql` is False, don't execute any SQL statements.
    Instead, perform a dry run: Just print SQL statements to stdout.

    Return the name of the new test DB (or `None` if no new DB was created).
    """
    if not execute_sql:
        # If `execute_sql` is False, we instead perform a dry run:
        # Just print SQL statements to stdout, don't execute any SQL.
        _init_db_tables(None)
        return None
    else:
        if db_name is None:
            # Use the default `_TEST_DB_NAME`.
            db_name = _TEST_DB_NAME

        # If the database doesn't exist, this will raise:
        #   psycopg2.OperationalError: FATAL:  database "noexist" does not exist
        #
        # If your username doesn't have permission to access the database,
        # this will raise:
        #   psycopg2.OperationalError: FATAL:  role "{username}" does not exist
        db_con = psycopg2.connect(dbname=db_name)
        print('Connected to test DB: %s\n' % db_name)

        _check_for_existing_db_tables(db_con, drop_existing_tables)

        _init_db_tables(db_con)
        db_con.close()
        return db_name


def _check_for_existing_db_tables(db_con, drop_existing_tables):
    # We don't want to insert data into existing tables, for 2 reasons:
    #  1. The tables might have been created for a different purpose,
    #   with a different table schema.
    #  2. Even if the tables have the appropriate schema, if they were
    #   created during a different run of this test, then they will
    #   already be populated with the test data.  Inserting more data
    #   into the test tables will confuse the test results.
    #
    # So, if we encounter existing tables with the tables-to-test names in
    # this test database, we cannot simply continue.  We have 2 options: 
    #  1. Drop the tables / delete their data.
    #  2. Abort the test with a complaint, until the caller drops/deletes
    #   the tables manually.
    #
    # We would prefer not to drop/delete test tables that already exist,
    # because they might contain test data from a previous test run that
    # the caller intended to inspect.  We should certainly not drop/delete
    # these tables silently.
    #
    # Hence, the parameter `drop_existing_tables` to this function:
    #  - If `False`, raise a `ValueError` because the table already exists.
    #  - If `True`, warn that the table already exists; then drop the table;
    #   then continue to create the table as normal.

    list_of_tables_to_check = ", ".join([
            # Recall that SQL only accepts single-quoted strings,
            # not double-quoted (like in Python and Shell).
            # So we can't simply use `repr()` here to quote the string.
            #
            # NOTE: The table name string must be converted to lowercase
            # or else it won't match the name in the database.
            ("'%s'" % table_name.lower()) for table_name in _TABLES_TO_TEST])

    db_cur = db_con.cursor()
    db_cur.execute(
            """SELECT table_name
            FROM information_schema.tables
            WHERE table_name IN (%s);"""
            % list_of_tables_to_check)
    any_existing_tables = db_cur.fetchall()

    if any_existing_tables:
        # `any_existing_tables` is a list of single-element tuples of a string.
        #
        # Each table in the list is represented by its table name as a string,
        # within a single-element tuple.
        list_of_existing_tables = ", ".join([t for (t,) in any_existing_tables])
        if drop_existing_tables:
            for (table_name,) in any_existing_tables:
                warn('existing table will be dropped: %s' % table_name)

            db_cur.execute("""DROP TABLE %s;""" % list_of_existing_tables)
            db_con.commit()
        else:
            raise ValueError('existing tables: %s' % list_of_existing_tables)

    db_cur.close()


def _init_db_tables(db_con):
    # If `db_con` is None, we instead perform a dry run:
    # Just print SQL statements to stdout, don't execute any SQL.
    db_cur = (db_con.cursor() if db_con is not None else None)
    _create_tables(db_cur)
    _insert_values(db_cur)

    if db_cur is not None:
        db_cur.close()


def _create_tables(db_cur):
    # If `db_cur` is None, we instead perform a dry run:
    # Just print SQL statements to stdout, don't execute any SQL.
    execute_sql = (db_cur.execute if db_cur is not None else print)

    column_defs = ',\n    '.join(
            [col_name for (col_name, col_values) in _COLUMNS_TO_TEST])
    for table_name in _TABLES_TO_TEST:
        create_sql = 'CREATE TABLE {table}(\n    {cols});\n'.format(
                table=table_name, cols=column_defs)
        execute_sql(create_sql)

    print('%d tables created\n' % len(_TABLES_TO_TEST))


def _insert_values(db_cur):
    # For predictability, we insert the same rows into every table;
    # but our "solution" code will update only *some* of these rows.
    # (Then we'll be able to perform a diff, to verify the updates.)
    #
    # This next variable is a list of all the row tuples to insert.
    #
    # The `zip()` transforms:
    #  - a list (foreach column) of lists (foreach pkey) of values
    # into:
    #  - a list (foreach pkey) of row-tuples
    foreach_pkey_row_tuple = list(zip(*
            [col_values for (col_name, col_values) in _COLUMNS_TO_TEST]))
    num_rows_per_table = len(foreach_pkey_row_tuple)

    # Convert each row-tuple into a string representation,
    # then join that list into a single long multiline string.
    # This enables us to insert all rows for a given table
    # in a single SQL statement.
    all_rows_multiline_string = ',\n    '.join([
            repr(tup) for tup in foreach_pkey_row_tuple])

    # If `db_cur` is None, we instead perform a dry run:
    # Just print SQL statements to stdout, don't execute any SQL.
    execute_sql = (db_cur.execute if db_cur is not None else print)

    total_num_rows_inserted = 0
    for table_name in _TABLES_TO_TEST:
        insert_sql = 'INSERT INTO {table} VALUES\n    {rows};\n'.format(
                table=table_name, rows=all_rows_multiline_string)
        execute_sql(insert_sql)
        total_num_rows_inserted += num_rows_per_table

    if db_cur is not None:
        db_cur.connection.commit()

    print('%d rows inserted in total (%d rows for each table)\n' %
            (total_num_rows_inserted, num_rows_per_table))


def warn(message):
    """Warn specified `message` to stderr."""
    print('Warning: %s' % message, file=sys.stderr)


if __name__ == '__main__':
    # If `execute_sql` is False, we instead perform a dry run:
    # Just print SQL statements to stdout, don't execute any SQL.
    create_new_test_db(execute_sql=True, drop_existing_tables=True)
