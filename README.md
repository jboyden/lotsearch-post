Post submission by James Boyden
===============================

This is a supplementary submission for the Lotsearch Skill Test, submitted
in addition to my "official" submission (which I submitted on Friday evening):
[https://gitlab.com/jboyden/lotsearch-test](https://gitlab.com/jboyden/lotsearch-test)

For this "Post-submission" submission, I thought it might be interesting
to create alternate solutions using **PostgreSQL** rather than SQLite in Q1;
and **PostgreSQL/PostGIS** rather than Shapefiles as the spatial data format
in Q2.

Question 1:
-----------

The solution for Question 1 is the same as in
[my original submission on Friday](https://gitlab.com/jboyden/lotsearch-test#question-1),
so please consult that original README for a description of the Python code
and my testing approach.  The difference in this submission is that I used
PostgreSQL rather than SQLite as the database.

The following Linux commands were required to be able to access the PostgreSQL
database installed on my system:

```
# To list the current tables in the Postgres database:
sudo -u postgres /usr/bin/psql --list

# To create a database user (a database role that has `LOGIN` attribute)
# with the same database username as the current Linux username:
sudo -u postgres /usr/bin/createuser --createdb ${USER}

# Now I should be able to rely upon Postgres "ident authentication" (which
# on a local Unix-socket connection, is treated as "peer authentication",
# which relies on the operating system to authenticate a user by username)
# to validate/verify my identity:
psql --list

# Now, as myself, create a new database `lotsearch_test`:
createdb --echo lotsearch_test

# We can verify that a new database `lotsearch_test` has been created,
# owned by my Linux username:
psql --list

# ... or even:
psql --list | grep lotsearch_test
# lotsearch_test | jboy     | UTF8     | en_AU.UTF-8 | en_AU.UTF-8 | 

# ... or even:
psql --list | grep ${USER}
# lotsearch_test | jboy     | UTF8     | en_AU.UTF-8 | en_AU.UTF-8 | 
```

Question 2:
-----------

The solution for Question 2 is the same as in
[my original submission on Friday](https://gitlab.com/jboyden/lotsearch-test#question-2),
so please consult that original README for a description of the Python code
and my testing approach.  The difference in this submission is that I used
PostgreSQL/PostGIS rather than Shapefiles as the output spatial data format.

To enable/activate the PostGIS extension in my PostgreSQL database,
it was necessary to execute the following Linux commands as the `postgres`
database owner/superuser:

```
$ sudo -u postgres /usr/bin/psql
postgres=# CREATE EXTENSION postgis;
postgres=# SELECT postgis_full_version();
```

Linux package dependencies
--------------------------

The following non-default packages were needed on my Linux Mint system:

- `gdal-bin`
- `markdown`
- `postgis`
- `postgresql`
- `postgresql-client`
- `postgresql-postgis`
- `python3-gdal`
- `python3-psycopg2`

These package dependencies are also listed in file `apt-get-install.txt`.
It should be convenient to install these packages using the command-line:

```
sudo apt-get install `cat apt-get-install.txt`
```

Python package dependencies
---------------------------

The following non-default Python packages were needed on my system:

- `requests`

This package dependency is also listed in file `requirements.txt`.
It should be convenient to install this package using the command-line:

```
sudo python3 -m pip install -r requirements.txt
```

(If you're running in a Python virtual environment, you won't need `sudo`
at the start of the command.)

