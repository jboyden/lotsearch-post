TIMESTAMP = $(shell date '+%Y-%m-%d--%H-%M-%S')

# Take a timestamped work-in-progress snapshot.
# (Normally I'd use git for this.)
snapshot:
	( cd .. && tar cjvf wip-snapshot--$(TIMESTAMP).tar.bz2 Post_submission_James_Boyden )

# Package up the final post submission.
tar: clean
	( cd .. && tar cvf post_submission_james_boyden.tar Post_submission_James_Boyden )

# Regenerate the README.html from markdown.
doc: README.md
	markdown README.md > README.html

# Install any Linux Mint package requirements.
install-apt:
	sudo apt-get install gdal-bin markdown postgis postgresql postgresql-client postgresql-postgis python3-gdal python3-psycopg2

# Install any Python3 package requirements.
install-pip: requirements.txt
	python3 -m pip install -r requirements.txt

# Delete any unnecessary (probably large) temporary files.
clean:
	rm -rf q1/__pycache__
	rm -f q2/_response.geojson
