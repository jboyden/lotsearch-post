#!/usr/bin/env python3
#
# The original specification in `Skill Test.docx`:
#
#       Write a python script that creates a shapefile from the Water Customer
#       Connection Points layer where the `D_Diameter = ‘100m’`.
#       
#       Please extract the data from the following Map Server end point.
#       
#       https://services.thelist.tas.gov.au/arcgis/rest/services/Public/Infrastructure/MapServer/44
#       
#       Output spatial reference must be 7855.
#
# and Nigel subsequently clarified in email:
#      
#       Please proceed with `D_Diameter = '100mm'`
#
# Usage:
#   python3 solution_q2.py
#
# Running this script will create a new table `ogrgeojson` in the
# specified PostgreSQL database, which will contains the GeoJSON content
# from `_response.geojson` (a GeoJSON file that caches the response).

import json
import os
import psycopg2
import requests

# This is available from:
#  https://svn.osgeo.org/gdal/trunk/gdal/swig/python/samples/ogr2ogr.py
import ogr2ogr

from osgeo import gdal


# The URL of the Map Server endpoint to query.
#
# (If this were a longer-lived script, I'd set up command-line parsing
# using `argparse`, so this could be specified on the command-line.)
_ENDPOINT_URL = 'https://services.thelist.tas.gov.au/arcgis/rest/services/Public/Infrastructure/MapServer/44'

# The output spatial reference to request.
#
# (Yadda yadda `argparse`.)
_SPATIAL_REF = 7855

# The filename of the output Shapefile to create.
#
# (Yadda yadda `argparse`.)
_DB_NAME = 'lotsearch_test'

# The query parameters for a GET method request.
_QUERY_PARAMS = dict(
        where="D_DIAMETER='100mm'",
        outFields="*",
        returnGeometry=True,
        outSR=_SPATIAL_REF,
        returnIdsOnly=False,
        returnCountOnly=False,
        returnExtentOnly=False,
        #f="pjson",
        f="geojson",
)

# The filename of a GeoJSON (JSON) file in which to cache the response.
# (When you're web-scraping, it's polite to cache the intermediate responses
# so you don't need to hit the remote server any more than necessary.)
_CACHED_JSON_FNAME = '_response.geojson'


def demo_solution():
    if os.path.exists(_CACHED_JSON_FNAME):
        print('Loading cached JSON response: %s' % _CACHED_JSON_FNAME)
        with open(_CACHED_JSON_FNAME) as f:
            resp_json = json.load(f)
    else:
        print('Querying Map Server endpoint: %s' % _ENDPOINT_URL)
        resp_json = _query_map_server(_ENDPOINT_URL, _QUERY_PARAMS)
        print('Saving (caching) JSON response to file: %s' % _CACHED_JSON_FNAME)
        with open(_CACHED_JSON_FNAME, 'w') as f:
            # For reproducability, sort the output of dictionaries by key.
            # Otherwise, even for identical data, the ordering of the keys
            # in a saved dictionary fluctuates randomly,
            # which complicates text-based diffing of saved JSON data.
            json.dump(resp_json, f, indent=1, sort_keys=True)

    _inspect_response_features(resp_json)
    _insert_geojson_into_postgres(_DB_NAME, _CACHED_JSON_FNAME)


def _insert_geojson_into_postgres(db_name, geojson_fname):
    """Insert GeoJSON file named `geojson_fname` into a Postgres DB `db_name`.

    This will create a new table `ogrgeojson` in PostgreSQL DB `db_name`
    which contains the GeoJSON content.

    This uses GDAL's `ogr2ogr.py`:
     https://svn.osgeo.org/gdal/trunk/gdal/swig/python/samples/ogr2ogr.py
    """
    # Firstly, let's silently drop table `ogrgeojson` if it already exists,
    # or else we'll get this error:
    #
    #   FAILED: Layer OGRGeoJSONalready exists, and -append not specified.
    #           Consider using -append, or -overwrite.
    #   Terminating translation prematurely after failed
    #   translation of layer OGRGeoJSON (use -skipfailures to skip errors)
    db_con = psycopg2.connect(dbname=db_name)
    db_cur = db_con.cursor()
    # NOTE: The `IF EXISTS` option to `DROP TABLE` is a PostgreSQL extension:
    #  https://www.postgresql.org/docs/current/sql-droptable.html
    table_name = 'ogrgeojson'
    db_cur.execute('DROP TABLE IF EXISTS %s;' % table_name)
    db_con.commit()
    db_cur.close()

    # Silence the following error message from OGR:
    #   ERROR 6: EPSG PCS/GCS code 7855 not found in EPSG support files.
    #   Is this a valid EPSG coordinate system?
    gdal.PushErrorHandler('CPLQuietErrorHandler')

    # NOTE: Don't forget a dummy string to fill the place of `sys.argv[0]`.
    argv = [
            'dummy sys.argv[0]',
            '-f',
            'PostgreSQL',
            'PG:dbname=%s' % db_name,
            geojson_fname,
    ]
    ogr2ogr.main(argv)
    # You can verify the existence of this new table by connecting to the
    # database `db_name` using `psql` and executing this SQL statement:
    #
    #   SELECT table_name
    #   FROM information_schema.tables
    #   WHERE table_name = 'ogrgeojson';
    #
    # You can count the number of Point geometries inserted by executing:
    #
    #   SELECT count(*) FROM ogrgeojson;
    #
    # There should be 1006 Point geometries in the table.
    # You can review the first 10 Point geometries by executing:
    #
    #   SELECT * FROM ogrgeojson LIMIT 10;


def _inspect_response_features(resp_json):
    """Briefly describe the response features for user information."""
    features = resp_json['features']
    num_features = len(features)
    print('%d features extracted' % num_features)
    if num_features > 0:
        print('features[0] =\n%s' % json.dumps(features[0], indent=1))


def _query_map_server(endpoint_url, query_params):
    """Query the Map Server endpoint specified by `endpoint_url`.

    Provide the query parameters specified in dict `query_params`.

    Return the parsed JSON response (as Python dicts, etc.).
    """
    resp = requests.get('%s/query' % endpoint_url, params=query_params)
    print('Response URL: %s\n' % resp.url)
    return resp.json()


if __name__ == '__main__':
    demo_solution()
